#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  9 17:21:59 2018

@author: lauracoelho
"""

#0
my_tuple=('a','b','c')

#1
tup=(1,2,3)
tup(0)=1 #Error -> tuples aren't meant to be changed

#2
apple_tuple=(100, 101, 102, 103, 104)
apple_tuple.sort(reverse=True) #Error ->

#4
apple_list=[100,101,102,103,104]
apple_list.append(105)
                  
#9
my_dict={'Hello':'World', 'Olá':'Mundo!'}

#11
list(1,2,3)
tuple([1,2,3])
dict(a=10)

#12
my_dict={'hello':1,'hi':2, 'hey':3}
del my_dict['hello']
my_dict.pop('hi')

#13
def list_append(a_list):
    a_list.append('homie')
    return a_list
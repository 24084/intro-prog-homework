#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 11:16:33 2018

@author: lauracoelho
"""
#1
bool(1)
#True

bool(1.1)
#True

bool(0)
#False

bool(0.0001)
#True

bool("")
#False

bool(None)
#False

#2
def ticker_symbol(stock):
    if stock=='APPL':
        return True
    else:
        return False

#3
def gender_color(gender):
    if gender=='Male':
        return 'You like pink'
    elif gender=='Female':
        return 'You hate pink'

#4 (?)
def gender(gender):
    if gender=='male':
        print('You hate pink')
    elif gender=='female':
        print('You like pink')
    else:
        print('This line')
        
#5
def age (age_m, age_d):
    if age_m==age_d:
        return age_m+age_d
    else:
        return age_m-age_d

#6
def size(my_dict, my_list):
    if len(my_dict)==len(my_list):
        return 'Awesome'
    else:
        return 'That is sad'

my_list=[1,2,3]

my_dict={'1':1,'2':2,'3':3}

#7
stocks={'APPL':1, 'GOOG':2, 'FB':3}

for key in stocks:
     print(key)

for key in stocks:
    print(stocks[key])

for key in stocks:
    print('The stock price of',key,'is',stocks[key])
    
#8
my_tuple=(10,20,30)
other_list=[40,50,60]
other_dict={
        'APPL':100,
        'GOOG':90,
        'FB':80
        }

for num in my_tuple:
    print(num)

for num in other_list:
    print(num)

for key in other_dict:
    print(other_dict[key])

#9
def mult_list(my_list):
    new_list = []
    for i in my_list:
        new_list.append(i * 2)
    return new_list

#10
my_dict={
        'APPL':100,
        'GOOG':90,
        'FB':80
        } 
def mult_dict(my_dict):
    new_dict={}
    for key in my_dict:
        new_dict[key]=my_dict[key]*2
    return new_dict

#11
def last(my_list,a_num):
    if len(my_list)>a_num:
        return True

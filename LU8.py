#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 16:36:42 2018

@author: lauracoelho
"""

#1
class Robot:
    
    def __init__(self):
        self.recordings=[]
        
    def listen(self,string):
        self.recordings.append(string)
    
    def play_recordings(self):
        print(self.recordings)
        
robot=Robot()
r=Robot()

robot.listen('ola')
robot.listen('adeus')

r.listen('hi')
r.listen('bye')

robot.play_recordings()

r.play_recordings()


#2
class Robot:
    
    def __init__(self):
        self.recordings=[]
        
    def listen(self,string):
        self.recordings.append(string)
    
    def play_recordings(self):
        print(self.recordings)
        
    def delete_recordings(self):
        self.recordings.clear()
      
robot=Robot()
r=Robot()
    
r.listen('heya')
r.delete_recordings()
r.play_recordings()
r.listen('what the hell')
r.play_recordings()

#4
          
class LectureRoom:
    def __init__(self):
        self.capacity=40
        
    def increase_capacity(self, amount):
        if self.capacity+amount<100 and self.capacity+amount>10:
            self.capacity=self.capacity+amount
        else:
            print("Error - you cannot increase to that capacity")
            
    def decrease_capacity(self, amount):
        if self.capacity-amount>10 and self.capacity-amount<100:
            self.capacity=self.capacity-amount
        else:
            print("Error - you cannot decrease to that capacity")
            
class_1=LectureRoom()
class_1.increase_capacity(80)
print(class_1.capacity)
class_1.decrease_capacity(10)
print(class_1.capacity)
class_1.decrease_capacity(40)
print(class_1.capacity)
   
# OR #     

class LectureRoom:
    
    def __init__(self):
        self.capacity=40

    def valid_capacity(self, new_capacity):
        if 10 > new_capacity or new_capacity > 100:
            print('invalid capacity, must be between 10 and 100')
            return False
        else:
            return True

    def increase_capacity(self, amount):
        if self.valid_capacity(self.capacity + amount):
            self.capacity += amount

    def decrease_capacity(self, amount):
        if self.valid_capacity(self.capacity - amount):
            self.capacity -= amount

class_1=LectureRoom()
class_1.increase_capacity(80)
print(class_1.capacity)
class_1.decrease_capacity(10)
print(class_1.capacity)
class_1.decrease_capacity(40)
print(class_1.capacity)
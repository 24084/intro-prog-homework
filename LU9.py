#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 16:50:06 2018

@author: lauracoelho
"""
#1 
   
class LectureRoom:
    
    def __init__(self):
        self.capacity=40
        self.max_capacity=100
        self.min_capacity=10
        
    def valid_capacity(self, new_capacity):
        if self.min_capacity > new_capacity or new_capacity > self.max_capacity:
            print('invalid capacity, must be between', 
                  self.min_capacity,'and',self.max_capacity)
            return False
        else:
            return True

    def increase_capacity(self, amount):
        if self.valid_capacity(self.capacity + amount):
            self.capacity += amount

    def decrease_capacity(self, amount):
        if self.valid_capacity(self.capacity - amount):
            self.capacity -= amount

class BigRoom(LectureRoom):
    
    def __init__(self):
        self.capacity=40
        self.max_capacity=200
        self.min_capacity=50

class SmallRoom(LectureRoom):
    
    def __init__(self):
        self.capacity=10
        self.max_capacity=20
        self.min_capacity=5
      
br=BigRoom()
br.increase_capacity(70)
print(br.capacity)

sr=SmallRoom()
sr.increase_capacity(50)

                                        ###

#2

class Robot:
    def __init__(self):
        self.recordings=[]
    
    def listen(self, string):
        self.recordings.append(string)
    
    def play_recordings(self):
        print(self.recordings)
    
    def delete_recordings(self):
        self.recordings=[]
    

class FlameThrower(Robot):
    
    def throw_flames(self):
        print('Burn, baby! Burn!!!')
        



class Tank(FlameThrower):
    
    def throw_flames(self):
        print("Throws VERY LARGE flame")
        self.delete_recordings()
    
    def hulk_stomp(self):
        print("Hulk stomping everywhere!")
        
        
        #OR#
    
#class FlameThrower(Robot):
    
    #def __init__(self):
        #self.flame='Burn, baby! BURN!!!'
    
   #def throw_flames(self):
        #print(self.flame)
        #self.delete_recordings()
        
        
#class Tank(FlameThrower):
    
    #def __init__(self):
        #self.flame='Throws VERY LARGE flame'
    
    #def hulk_stomp(self):
        #print('Hulk stomping everywhere!')

t=Tank()
t.throw_flames()
t.hulk_stomp()

                                ###

#3

class Robot:
    def __init__(self):
        self.recordings=[]
    
    def listen(self, string):
        self.recordings.append(string)
    
    def play_recordings(self):
        print(self.recordings)
    
    def delete_recordings(self):
        self.recordings=[]


class LightRobot(Robot):
    
    def listen(self, string):
        super().listen(string)
        if len(self.recordings)>=3:
            del self.recordings[0]
            
class FlameThrower(LightRobot):
    
    def throw_flames(self):
        print('Burn, baby! Burn!!!')
        self.delete_recordings()
        
class CryingRobot(LightRobot):
    def listen(self, string):
        super().listen(string)
        if len(self.recordings) >= 3:
            print('Disk is soooooo fullllll. Call the ambulance!')

class FlyingRobot(LightRobot):
    def fly(self):
        print('I am flying gracefully in the clouds!')
        
        if self.recordings:
            del self.recordings[-1]
        
        #if self.recordings:
            #self.recordings.pop()

l=LightRobot()
l.listen('hello')
l.play_recordings()
l.listen('helloo')
l.play_recordings()
l.listen('hellooo')
l.play_recordings()
l.listen('hey')
l.play_recordings()
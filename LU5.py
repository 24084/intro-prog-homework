#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  9 11:50:20 2018

@author: lauracoelho
"""
#4
global_var=4

def global_func():
    return global_var*2

result=global_func()

print(global_var)
print(result)


#5
power=10
print(power)

def generate_power(number):
   # _power_=2
    
    def nth_power():
        return number ** power
    
    powered=nth_power()
    return powered

# print(_power_)
a=generate_power(2)
print(a)

#6
power=10
print(power)

def generate_power(number):
    power=2
    
    def nth_power():
        return number ** power
    
    powered=nth_power()
    return powered

print(power)
a=generate_power(2)
print(a)

#7
_what='coins'
_for='chocolates'

def transaction(_what,_for):
    _what='oranges'
    _for='bananas'
    print(_what,_for)
    
transaction(_what,_for)
print(_what,_for)

#9
def name(first_name='laura',last_name='coelho'):
    print(first_name,last_name)

name()
name(last_name='inacio')
name('joana','maria')

#10
first_name='Ricardo'
last_name='Pereira'

def names(first_name='Sam',last_name='Hopkins'):
    print(first_name,last_name)
    
names()
names(first_name=first_name,last_name=last_name)
names(first_name='Jose',last_name='Maria')

#11
def greetings(message='Good Morning',_name=''):
    print(message,_name)

greetings()
greetings(message='You know nothing',_name='John Snow')
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 16:09:29 2018

@author: lauracoelho
"""

from novamud import Dungeon, Room, Thing
    
class MetroTicket(Thing):
    name = 'MetroTicket'
    description = ("You'll need to purchase a ticket if you want to travel"
                   " somewhere by metro.")

class Alameda(Room):
    name = 'Alameda'
    description = ('The metro station that joins the Green and Red lines.'
                   ' Perhaps one of the busiest in the city. Located in the city'
                   ' centre. Cais is your destination.')

    def init_room(self):
        self.add_thing(MetroTicket())
        
    def go_to(self, player, other_room_name):
        if not player.carrying or player.carrying.name != 'MetroTicket':
            player.tell("You can't go anywhere without a ticket!")
        else:
            super().go_to(player, other_room_name)

class CaisMetro(Room):
    name = 'CaisMetro'
    description = ("You have finally reached the Cais do Sodré metro station."
                   " If you wish to go out at night, this is where you should"
                   " be. Unfortunately, you still have classes to attend."
                   " The train station is your destination.")

class TrainTicket(Thing):
    name = 'TrainTicket'
    desceription = ("You'll need to purchase a ticket if you want to travel"
                   " somewhere by train.")

class CaisTrain(Room):
    name = 'CaisTrain'
    description = ('The train station that takes you to the "linha" area of'
                   ' Lisbon. This is where the posh people live. You also'
                   ' happen to study at the posh school where the posh people'
                   ' live. How fun. Enjoy the beach (if you can...)! Oeiras'
                   ' is your destination.')

    def init_room(self):
        self.add_thing(TrainTicket())
        
    def go_to(self, player, other_room_name):
        if not player.carrying or player.carrying.name != 'TrainTicket':
            player.tell("You can't go anywhere without a ticket!")
        else:
            super().go_to(player, other_room_name)

class Train(Room):
    name = 'Train'
    description = ("The wonderful CP train and the Cascais line. You must run"
                   " over people if you intend on scoring a seat. Portuguese become"
                  " savage when it comes to finding a seat in public transports.")

class Oeiras(Room):
    name = 'Oeiras'
    description = ("The amazing Oeiras station. There's absolutely nothing"
                   " surrounding it, wow! Breathtaking! You might want to run,"
                   " it seems like the perfect place to be mugged or kidnapped..."
                   " Don't forget to swipe your ticket!")
    
    def register_commands(self):
        return ['swipe_card']

    def add_player(self, player):
        player.ticket = False
        super().add_player(player)

    def swipe_card(self, player):
        player.ticket = True
        player.tell('~ BEEP ~ You have successfully swiped your card.')
        
    def swipe_card_describe(self):
        return "You must swipe your ticket before you go Outside."

    def go_to(self, player, other_room_name):
        if not player.ticket == True:
            player.tell("You can't go anywhere without swiping your ticket!")
        else:
            super().go_to(player, other_room_name)

class Outside(Room):
    name = 'Outside'
    description = ("Now a 15 minute walk awaits you. You should consider"
                   " cancelling that gym subscription, with all the walking"
                   " you'll be doing...")
    
    def register_commands(self):
        return ['walk']

    def walk_describe(self):
        return "You must walk to Nova. Remember, it takes 15 minutes to get there."

    def add_player(self, player):
        player.walk_distance = 0
        super().add_player(player)

    def walk(self, player):
        player.walk_distance += 1
        if player.walk_distance == 1:
            player.tell('You have just walked for 1 minute straight.')
        elif 1 <= player.walk_distance < 8:
            player.tell(
                "You have walked for {} minute. Seriously, tired already?".format(
                    player.walk_distance
                    )
            )
        elif 8 <= player.walk_distance < 14:
            player.tell(
                "You have walked for {} minutes, you're almost there!".format(
                    player.walk_distance
                    )
            )
        elif player.walk_distance >= 15:
            player.tell(
                "You've just arrived. Finally! Wheew, that was tiring... I'm"
                " sure you've 'lost' some of your belonging along the way..."
            )

    def go_to(self, player, other_room_name):
        if not player.walk_distance == 15:
            player.tell("Your stamina sucks, you couch potato! You still have"
                        "some walking to do.")
        else:
            super().go_to(player, other_room_name)
            
class HovioneAtrium(Room):
    name = "HovioneAtrium"
    description = ("The gorgeous Hovione Atrium, where your have a moving glass"
                   " roof. Isn't that amazing?! It just takes a semester to open!"
                   " Expect a tsunami to happen when it rains and the roof is open."
                   " #ClearHorizon   Don't forget to message Sam on Slack!")

    def register_commands(self):
        return ['message_sam']

    def message_Sam_describe(self):
        return "You must send a message to Sam on Slack so that he opens the classroom door."
    
    def add_player(self, player):
        player.message = False
        super().add_player(player)

    def slack_message(self, player):
        player.message = True
        player.tell("You've sent a message successfully. Sam will open the door now.")

    def go_to(self, player, other_room_name):
        if not message == True:
            player.tell("The door is closed. Did you forget to message Sam on Slack?")
        else:
            super().go_to(player, other_room_name)

class Classroom(Room):
    name = 'Classroom'
    description = ("You've finally made it to Sam's class! If you're lucky,"
                   " you're not underground, nor with a view towards the builders,"
                   " the internet is working, and so is Moodle. Perfect learning"
                   " environment! So, sit down and learn a bunch of stuff!")

    def register_commands(self):
        return ['learn_bunch_of_stuff']

    def learn_bunch_of_stuff_describe(self):
        return ("Learning is so much fun. Yay. I'm feeling smarter by the second."
                " How smart can I get?")
    
    def add_player(self, player):
        player.smart_level = 0
        super().add_player(player)

    def learn_bunch_of_stuff(self, player):
        player.smart_level += 1
        if player.smart_distance == 1:
            player.tell("Horaay! You've learnt something knew. You're {}% smarter!"
                        " Still pretty dumb...".format(player.smart_level))
        elif player.smart_level == 2:
            player.tell("Horaay! You've learnt something knew. You're {}% smarter!"
                        " Almost a programming expert!".format(player.smart_level))
        
        elif player.smart_level == 3:
            player.tell("Horaay! You've learnt something knew. You're {}% smarter!"
                        " You've just hacked into the US Government!".format(player.smart_level))
        
        elif 3 < player.smart_level <= 4:
            player.tell("You're {}% smarter! We get it, you're smart!".format(player.smart_level))            

class NovaWayOfLife(Dungeon):
    
    name = 'NovaWayOfLife'
    description = ('The challenging path that awaits you every single day since'
                   ' the school decided it was too good to stay in Lisbon.'
                   ' Are you still sure you made the right decision doing your'
                   ' Masters at Nova?!')
    
    def init_dungeon(self):
        a = Alameda(self)
        cm = CaisMetro(self)
        ct = CaisTrain(self)
        t = Train(self)
        o = Oeiras(self)
        out = Outside(self)
        ha = HovioneAtrium(self)
        cr = Classroom(self)
        
        a.connect_room(cm, two_way=True)
        cm.connect_room(ct,two_way=True)
        ct.connect_room(t,two_way=True)
        t.connect_room(o, two_way=True)
        o.connect_room(out, two_way=True)
        out.connect_room(ha, two_way=True)
        ha.connect_room(cr, two_way=True)
        
        self.start_room = a
        
       
if __name__ == '__main__':
    NovaWayOfLife().start_dungeon()               
    
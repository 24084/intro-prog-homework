#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 15:38:27 2018

@author: lauracoelho
"""

def my_func(n):
    ret_val=n/2
    return ret_val

print(my_func(6))

n_divided = my_func(8)

print(n_divided)

def hello(n):
    return n/2

print(hello(6))


def divide_by_two(n):
    ret_val = n/2
    return ret_val

a=2
b=divide_by_two(a)
print('The value of a/2 is',b)

def power(n,m):
    return n**m

def equal_parameters(n,m):
    return n==m

def hypothenuse_pythagoras(n,m):
    side_1=(n**2)
    side_2=(m**2)
    hypothenuse=side_1+side_2
    return hypothenuse**(0.5)

print(hypothenuse_pythagoras(3,4))

def bitcoin_to_litecoin(b):
    return b*114.64

print(bitcoin_to_litecoin(2))

def bitcoin_to_ethereum(b):
    return b*30.32

print(bitcoin_to_ethereum(2))

def bitcoin_to_euro(b):
    return b*5444.54

print(bitcoin_to_euro(2))



def somar(a,b):
    return

print(somar(1,2))


def imprimir(a):
    return print(a)

imprimir(1)


def retornar(a):
    return a

retornar(50)
